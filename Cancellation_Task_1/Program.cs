﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Cancellation_Task_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //in order to do the cancellation of a task, you need two things
            //a token and a token source

            //creating a token source that connects to a token
            CancellationTokenSource token_source = new CancellationTokenSource();

            //creating a token that connects to the token source
            CancellationToken token = token_source.Token;

            //lets start a task and attach the token to that thask

            Task about_to_be_cancelled_task = Task.Run(() =>
            {
                //until the token has received this loop will keep running
                while (token.IsCancellationRequested == false)
                {
                    Console.WriteLine("cancellation not yet requested. So I will keep jogging");
                    Thread.Sleep(100);
                }

                //raise the cancelled exception
                token.ThrowIfCancellationRequested();
            },token  //this is where I am connecting this task to a token
                );

            //I will latch on to the cancellation (which also means its completed, and continue to do some other stuff
            about_to_be_cancelled_task.ContinueWith((x) =>
            {
                Console.WriteLine("The task has been successfully cancelled!");
            });

            
            
            try
            {
                //lets get some input to cancel the task
                Console.WriteLine("enter anything to cancel the task");
                Console.ReadLine();
                token_source.Cancel();//this sends a cancellation request which then sends it to token which then tells the task that it is cancelled


                Console.WriteLine("You have cancelled the task");
                Console.ReadLine();

                //once the thread of UI is completed, then kick start the task
                about_to_be_cancelled_task.Wait();
            }
            catch(AggregateException e)
            {
                Console.WriteLine("went wrong - {0}",e.InnerExceptions[0].Message);
            }

            Console.WriteLine("All done");
            Console.ReadLine();


        }
    }
}
